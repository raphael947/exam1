<?php

use yii\db\Migration;

/**
 * Class m180624_054145_task
 */
class m180624_054145_task extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
{
        $this->createTable('task', [
            'id' => $this->primaryKey(),
            'name'=> $this->string(),
           // 'email' => $this->string(),
            'urgency' => $this->string(),
          
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);
 
    }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180624_054145_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180624_054145_task cannot be reverted.\n";

        return false;
    }
    */
}
