<?php

use yii\db\Migration;

/**
 * Class m180624_054004_user
 */
class m180624_054004_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
{
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'name'=> $this->string(),
           // 'email' => $this->string(),
            'username' => $this->string()->unique(),
            'auth_key' => $this->string(),
            'password' => $this->string(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);
 
    }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180624_054004_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180624_054004_user cannot be reverted.\n";

        return false;
    }
    */
}
