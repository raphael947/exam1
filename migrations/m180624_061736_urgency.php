<?php

use yii\db\Migration;

/**
 * Class m180624_061736_urgency
 */
class m180624_061736_urgency extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('urgency', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180624_061736_urgency cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180624_061736_urgency cannot be reverted.\n";

        return false;
    }
    */
}
